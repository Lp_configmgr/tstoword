﻿using System;
using System.Windows;
using System.Reflection;
using WordLibrary = Microsoft.Office.Interop.Word;
using Microsoft.Win32;
using System.IO;
using System.Xml.Linq;
using EzConfigMgr;
using System.Collections;
using Microsoft.ConfigurationManagement.ManagementProvider;
using System.Collections.Generic;
using System.Management;
using System.Windows.Input;
using System.Linq;

namespace TsToWord
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Dictionary<string, string> referenceDict = new Dictionary<string, string>();
        public Primary cmPrimary;
        public string lastElement = "group";
        public object oEndOfDoc = "\\endofdoc"; /* \endofdoc is a predefined bookmark */

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

        public void SetSize()
        {
            Width = 550;
            Height = 150;

            //New height for Primary site Connection
            if (Properties.Settings.Default.RadioCMgr) { Height = Height + 40; }
        }

        public void CreateNode(XElement Node, int header, WordLibrary._Document oDoc, string lastItem)
        {
            //Style management
            int currentHeader = header;
            switch (Node.Name.ToString())
            {
                case "step":
                    {
                        currentHeader = -75;
                        break;
                    }
                case "group":
                    {
                        //Set new header for future sub group
                        header--;
                        break;
                    }
            }

            //Set all group lower than Header 9 to Header 9
            if (header < -10) { currentHeader = -10; }

            //continueOnError add on title if check
            String continueOnError = "";
            if (Node.Attribute("continueOnError") != null && Properties.Settings.Default.AddContinueOnError)
            { continueOnError = " (continueOnError)"; }
            bool showGS = Node.Attribute("disable") == null || (Properties.Settings.Default.AddDisableGS && Node.Attribute("disable") != null);

            //Fill Document depending on Node Name
            switch (Node.Name.ToString())
            {
                case "group":
                case "step":
                    {
                        //Show step if step are note disable or if step is disabled but settings is check
                        if (showGS)
                        {
                            //insert Page Break before new group if last Element is a step
                            if (Properties.Settings.Default.AddPBreak && lastItem == "step" && Node.Name.ToString() == "group") { oDoc.Words.Last.InsertBreak(WordLibrary.WdBreakType.wdPageBreak); }

                            //Set new last element
                            lastElement = Node.Name.ToString();

                            //Create Title
                            WordLibrary.Paragraph oPara;
                            object endRange = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                            oPara = oDoc.Content.Paragraphs.Add(ref endRange);
                            oPara.Range.Text = Node.Attribute("name").Value.ToString() + continueOnError;
                            oPara.Range.set_Style(currentHeader);
                            oPara.Range.InsertParagraphAfter();

                            //Create Description
                            endRange = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                            oPara = oDoc.Content.Paragraphs.Add(ref endRange);
                            oPara.Range.Text = Node.Attribute("description").Value.ToString();
                            oPara.Range.set_Style(-1);
                            oPara.Range.InsertParagraphAfter();

                            //Create step Specificity
                            if (Node.Name.ToString() == "step")
                            {                                
                                string type = Node.Attribute("type").Value.ToString();
                                endRange = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                                string text = "";

                                //depend of node type

                                switch (type)
                                {
                                    case "SMS_TaskSequence_RunCommandLineAction":
                                        if (Properties.Settings.Default.AddCommandLine)
                                        {
                                            text += "Command :";
                                            IEnumerable<XElement> node = (from dvlists in Node.Descendants("defaultVarList").Descendants("variable")
                                                             where (string)dvlists.Attribute("name") == "CommandLine"                                                              
                                                             select dvlists);
                                            string cmd = node.First().Value.ToString();
                                            text += Environment.NewLine + cmd;
                                        }
                                        break;
                                }

                                if (!String.IsNullOrEmpty(text))
                                {
                                    oPara = oDoc.Content.Paragraphs.Add(ref endRange);
                                    oPara.Range.Text = text;
                                    oPara.Range.set_Style(-1);
                                    oPara.Range.InsertParagraphAfter();
                                }
                            }

                        }
                        break;
                    }
            }

            //If Node has Element we explore the tree and if disable we stop digging
            if (Node.HasElements && showGS)
            {
                foreach (XElement child in Node.Elements()) { CreateNode(child, header, oDoc, lastElement); }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            bool creation = RadioXml.IsChecked ?? false;
            XElement tsXml = null;
            string headerName= "Documentation Task Sequence ";

            if (creation)
            {
                tsXml = XElement.Load(Filebox.Text);
                headerName += Path.GetFileNameWithoutExtension(Filebox.Text);
            }
            else
            {
                tsXml = ((TaskSequence)CbTS.SelectedItem).Sequence;
                headerName += ((TaskSequence)CbTS.SelectedItem).Name;
            }

            WordLibrary._Application oWord;

            //Start Word and create a new document.
            object missing = System.Reflection.Missing.Value;
            object readOnly = false;
            object isVisible = true;
            object templatePath = Path.Combine(Environment.CurrentDirectory, @"Template\", "Template.dotx");

            oWord = new WordLibrary.Application();
            oWord.Visible = true;
            WordLibrary._Document oDoc = oWord.Documents.Add(ref missing, ref missing, ref missing, ref missing); ;

            try
            { 
                oDoc = oWord.Documents.Open(ref templatePath, ref missing, ref readOnly, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref isVisible, ref missing, ref missing, ref missing, ref missing);
            }
            catch
            {
                (new Info("Word already open.",3)).ShowDialog();
                System.Environment.Exit(20);
            }

            //Create Introduction
            WordLibrary.Paragraph oPara;
            object endRange = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oPara = oDoc.Content.Paragraphs.Add(ref endRange);
            oPara.Range.Text = "Introduction";
            oPara.Range.set_Style(-2);
            oPara.Range.InsertParagraphAfter();
            oDoc.Words.Last.InsertBreak(WordLibrary.WdBreakType.wdPageBreak);

            //Create Prerequisite
            endRange = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oPara = oDoc.Content.Paragraphs.Add(ref endRange);
            oPara.Range.Text = "Prerequisite";
            oPara.Range.set_Style(-2);
            oPara.Range.InsertParagraphAfter();
            oDoc.Words.Last.InsertBreak(WordLibrary.WdBreakType.wdPageBreak);


            //Create TaskSequence
            endRange = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oPara = oDoc.Content.Paragraphs.Add(ref endRange);
            oPara.Range.Text = "TaskSequence";
            oPara.Range.set_Style(-2);
            oPara.Range.InsertParagraphAfter();

            // -2 is Header 1
            CreateNode(tsXml, -3, oDoc, lastElement);

            //Add Reference if settings Check
            if (Properties.Settings.Default.AddReference)
            {
                oDoc.Words.Last.InsertBreak(WordLibrary.WdBreakType.wdPageBreak);

                //Create Title
                endRange = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                oPara = oDoc.Content.Paragraphs.Add(ref endRange);
                oPara.Range.Text = "Reference";
                oPara.Range.set_Style(-2);
                oPara.Range.InsertParagraphAfter();
                Dictionary<string, string> allList = new Dictionary<string, string>();

                oDoc.Words.Last.InsertBreak(WordLibrary.WdBreakType.wdLineBreak);

                //Get application
                var apps = from dvlists in tsXml.Descendants("defaultVarList")
                           where
                           (from var in dvlists.Descendants("variable")
                            where (string)var.Attribute("name") == "OSDApp"
                            select var).Any()
                           select dvlists;

                foreach (XElement app in apps)
                {
                    int nbapp = int.Parse((from varlist in app.Elements("variable")
                                  where (string)varlist.Attribute("name") == "OSDApp"
                                  select varlist).First().Value);

                    for (int i=0; i < nbapp; i++)
                    {
                        string name = ((from varlist in app.Elements("variable")
                                        where (string)varlist.Attribute("name") == "OSDApp" + i + "DisplayName"
                                        select varlist).First()).Value;
                        string reference = ((from varlist in app.Elements("variable")
                                             where (string)varlist.Attribute("name") == "OSDApp" + i + "Name"
                                             select varlist).First()).Value;
                        if (!allList.ContainsKey(reference))
                        {
                            allList.Add(reference, name);
                        }
                    }
                }

                //Get Package
                var pkgs = from dvlists in tsXml.Descendants("defaultVarList")
                           where
                           (from var in dvlists.Descendants("variable")
                            where (string)var.Attribute("name") == "OSDPackage"
                            select var).Any()
                           select dvlists;

                foreach (XElement pkg in pkgs)
                {
                    int nbpkg = int.Parse((from varlist in pkg.Elements("variable")
                                           where (string)varlist.Attribute("name") == "OSDPackage"
                                           select varlist).First().Value);

                    for (int i = 0; i < nbpkg; i++)
                    {
                        string name = ((from varlist in pkg.Elements("variable")
                                        where (string)varlist.Attribute("name") == "OSDPackage" + i + "Name"
                                        select varlist).First()).Value;
                        string reference = ((from varlist in pkg.Elements("variable")
                                             where (string)varlist.Attribute("name") == "OSDPackage" + i + "PackageId"
                                             select varlist).First()).Value;
                        if (!allList.ContainsKey(reference))
                        {
                            allList.Add(reference, name);
                        }
                    }
                }

                foreach (KeyValuePair<string, string>el in allList)
                {
                    //appName
                    endRange = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                    oPara = oDoc.Content.Paragraphs.Add(ref endRange);
                    oPara.Range.Text = el.Value;
                    oPara.Range.set_Style(-75);
                    oPara.Range.InsertParagraphAfter();

                    //reference
                    endRange = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                    oPara = oDoc.Content.Paragraphs.Add(ref endRange);
                    oPara.Range.Text = el.Key;
                    oPara.Range.set_Style(-1);
                    oPara.Range.InsertParagraphAfter();
                    oDoc.Words.Last.InsertBreak(WordLibrary.WdBreakType.wdLineBreak);
                }
            }

            bool TableOfContents = Properties.Settings.Default.AddToc;
            if (TableOfContents)
            {
                //add tables of content on the toc bookmark
                object tocRangeBm = "toc";
                WordLibrary.Range tocRange = oDoc.Bookmarks.get_Item(ref tocRangeBm).Range;
                var toc = oDoc.TablesOfContents.Add(Range: tocRange, UseHeadingStyles: true);

                //update Header
                toc.Update();
            }

            //Header
            WordLibrary.Section wordSection = oDoc.Sections[1];
            WordLibrary.Range HeaderRange = wordSection.Headers[WordLibrary.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
            HeaderRange.ParagraphFormat.Alignment = WordLibrary.WdParagraphAlignment.wdAlignParagraphCenter;
            HeaderRange.Text = headerName;

            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.Filter = "Word files (*.docx)|*.docx";
            if (saveFile.ShowDialog() == true)
            {
                object fileName = saveFile.FileName;

                oDoc.SaveAs(ref fileName,
                    WordLibrary.WdSaveFormat.wdFormatDocumentDefault, ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing, ref missing);
            }
            oDoc.Close();
            oWord.Quit();
            this.Close();
        }

        private void Select_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "xml files (*.xml)|*.xml";

            if (openFileDialog.ShowDialog() == true)
            {
                Filebox.Text = openFileDialog.FileName;
                Create.IsEnabled = true;
            }
        }

        private void RadioXml_Checked(object sender, RoutedEventArgs e) { SetSize(); }
        private void RadioCMgr_Checked(object sender, RoutedEventArgs e) { SetSize(); }

        //Save User Settings when software close
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            Properties.Settings.Default.Save();
            base.OnClosing(e);
        }

        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            cmPrimary = new Primary(TBPrimary.Text);
            IResultObject tempList;
            try
            {
                tempList = cmPrimary.GetTsList();
                List<TaskSequence> tsList = new List<TaskSequence>();
                foreach (IResultObject result in tempList)
                {
                    result.Get();
                    String strSequence = result["Sequence"].StringValue;
                    String packageID = result["PackageID"].StringValue;
                    String name = result["Name"].StringValue;
                    XElement sequence = XElement.Parse(strSequence);
                    TaskSequence ts = new TaskSequence(name, sequence);
                    tsList.Add(ts);
                }
                CbTS.ItemsSource = tsList;
                CbTS.UpdateLayout();
                CbTS.IsEnabled = true;
            }
            catch(Exception access)
            {
                (new Info(access.Message,3)).ShowDialog();
            }
        }

        private void CbTS_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            Create.IsEnabled = true;
        }

        private Version GetRunningVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version;
        }

        private void Title_Drag(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left) { DragMove(); }
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Minimize_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            (new Settings()).ShowDialog();
        }

        private void About_Click(object sender, RoutedEventArgs e)
        {
            (new about()).ShowDialog();
        }
    }
}