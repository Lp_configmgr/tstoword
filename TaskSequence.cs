﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace TsToWord
{
    class TaskSequence
    {
        public string Name { get; set; }
        public XElement Sequence { get; set; }

        public TaskSequence(string name, XElement sequence)
        {
            Name = name;
            Sequence = sequence;
        }
    }
}
