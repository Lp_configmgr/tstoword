﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TsToWord
{
    /// <summary>
    /// Logique d'interaction pour Info.xaml
    /// </summary>
    public partial class Info : Window
    {
        public Info(String contenu, int type = 1)
        {
            InitializeComponent();
            Text.Text = contenu;

            switch(type)
            {
                case 2: //Warning
                    TitleGrid.Background = new System.Windows.Media.SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFCC00"));
                    break;
                case 3: //Error
                    TitleGrid.Background = new System.Windows.Media.SolidColorBrush((Color)ColorConverter.ConvertFromString("#ff003b"));
                    break;
                case 4: //OK
                    TitleGrid.Background = new System.Windows.Media.SolidColorBrush((Color)ColorConverter.ConvertFromString("#00cc00"));
                    break;
                default: //Information
                    TitleGrid.Background = new System.Windows.Media.SolidColorBrush((Color)ColorConverter.ConvertFromString("#add8e6"));
                    break;
            }
        }

        private void Title_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
